package ru.vmaksimenkov.tm;

import ru.vmaksimenkov.tm.constant.ArgumentConst;
import ru.vmaksimenkov.tm.constant.TerminalConst;
import ru.vmaksimenkov.tm.model.Command;
import ru.vmaksimenkov.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        displayWelcome();
        if (run(args)) System.exit(0);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter command: ");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private static void displayHelp() {
        System.out.println(Command.HELP);
        System.out.println(Command.ABOUT);
        System.out.println(Command.VERSION);
        System.out.println(Command.INFO);
        System.out.println(Command.EXIT);
    }

    private static void displayVersion() {
        System.out.println("1.0.0");
    }

    private static void displayAbout() {
        System.out.println("Vlad Maximenkov");
        System.out.println("vmaksimenkov@tsconsulting.com");
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void incorrectCommand() {
        System.out.println("Unknown command. Type help to see all available commands");
    }

    private static void incorrectArgument() {
        System.out.println("Unknown argument. Type -h to see all available arguments");
    }

    private static boolean run(final String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private static void sysExit() {
        System.exit(0);
    }

    private static void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_HELP: displayHelp(); break;
            case ArgumentConst.ARG_VERSION: displayVersion(); break;
            case ArgumentConst.ARG_ABOUT: displayAbout(); break;
            case ArgumentConst.ARG_INFO: showSystemInfo(); break;
            default: incorrectCommand();
        }
    }

    private static void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_HELP: displayHelp(); break;
            case TerminalConst.CMD_VERSION: displayVersion(); break;
            case TerminalConst.CMD_ABOUT: displayAbout(); break;
            case TerminalConst.CMD_EXIT: sysExit(); break;
            case TerminalConst.CMD_INFO: showSystemInfo(); break;
            default: incorrectCommand();
        }
    }

    private static void showSystemInfo() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

}
